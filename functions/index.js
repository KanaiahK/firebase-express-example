const functions = require("firebase-functions");
let express = require("express");
let users = require("./users");
let devices = require("./devices");
let app = express();

app.use("(/apis)?/users", users);
app.use("(/apis)?/devices", devices);

app.get("*", function(req, res) {
  console.log(req);
  res
    .status(404) // HTTP status 404: NotFound
    .send(req.url + " not yet defined");
});

// Expose Express API as a single Cloud Function:
exports.apis = functions.https.onRequest((request, response) => {
  if (!request.path) {
    request.url = `/${request.url}`; // prepend '/' to keep query params if any
  }
  return app(request, response);
});
